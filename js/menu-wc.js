'use strict';


customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">UniConv app documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/ApplicationModule.html" data-type="entity-link">ApplicationModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ApplicationModule-ef45f3a44e398e1048471ce52d3d756f"' : 'data-target="#xs-components-links-module-ApplicationModule-ef45f3a44e398e1048471ce52d3d756f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ApplicationModule-ef45f3a44e398e1048471ce52d3d756f"' :
                                            'id="xs-components-links-module-ApplicationModule-ef45f3a44e398e1048471ce52d3d756f"' }>
                                            <li class="link">
                                                <a href="components/AllegatiComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AllegatiComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ApprovazionedetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ApprovazionedetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AssignmentDetailPageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AssignmentDetailPageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AziendaLocComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AziendaLocComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AziendeLocComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AziendeLocComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BolloRepertoriazioneComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BolloRepertoriazioneComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ClassificazioneComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ClassificazioneComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ClassificazioniComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ClassificazioniComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConvenzioneComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConvenzioneComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConvenzionedetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConvenzionedetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConvenzioniComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConvenzioniComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConvvalidationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConvvalidationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DocumentiTitulus.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DocumentiTitulus</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EmissioneComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EmissioneComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/EsecuzionedetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">EsecuzionedetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FaseWrapperComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FaseWrapperComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FirmaControparteComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FirmaControparteComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FirmaDirettoreComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FirmaDirettoreComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HomeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">HomeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InvioRichiestaPagamentoComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InvioRichiestaPagamentoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LogAttivitaComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LogAttivitaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MappingRuoli.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MappingRuoli</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MappingRuolo.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MappingRuolo</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MappingUfficiTitulus.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MappingUfficiTitulus</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MappingUfficioTitulus.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MappingUfficioTitulus</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MultistepSchematipoComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MultistepSchematipoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PagamentoComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PagamentoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PermissionComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PermissionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PermissionsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PermissionsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PersoneinterneTitulus.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PersoneinterneTitulus</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RangedetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RangedetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RepertoriazionedetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RepertoriazionedetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RichiestaEmissioneComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RichiestaEmissioneComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RoleComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RoleComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RolesComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RolesComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ScadenzaComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScadenzaComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ScadenzeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScadenzeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ScadenzedetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScadenzedetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SottoscrizioneComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SottoscrizioneComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SottoscrizionedetailsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SottoscrizionedetailsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StruttureEsterneTitulus.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StruttureEsterneTitulus</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StruttureInterneTitulus.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">StruttureInterneTitulus</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TaskComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TaskComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TasksComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TasksComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TipoPagamentiComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TipoPagamentiComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TipoPagamentoComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TipoPagamentoComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UploadfileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UploadfileComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserTaskDetailComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserTaskDetailComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UsersComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UsersComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-ApplicationModule-ef45f3a44e398e1048471ce52d3d756f"' : 'data-target="#xs-injectables-links-module-ApplicationModule-ef45f3a44e398e1048471ce52d3d756f"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ApplicationModule-ef45f3a44e398e1048471ce52d3d756f"' :
                                        'id="xs-injectables-links-module-ApplicationModule-ef45f3a44e398e1048471ce52d3d756f"' }>
                                        <li class="link">
                                            <a href="injectables/ApplicationService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ApplicationService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/AziendaLocService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AziendaLocService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/DocumentoService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>DocumentoService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LogAttivitaService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>LogAttivitaService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MappingRuoloService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MappingRuoloService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MappingUfficioService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MappingUfficioService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/PersonaInternaService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>PersonaInternaService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ScadenzaService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ScadenzaService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/StrutturaEsternaService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>StrutturaEsternaService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/StrutturaInternaService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>StrutturaInternaService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UnitaOrganizzativaService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UnitaOrganizzativaService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserTaskService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UserTaskService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link">AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-dd3a187f47aa5a0ce6d99caff64a1b4b"' : 'data-target="#xs-components-links-module-AppModule-dd3a187f47aa5a0ce6d99caff64a1b4b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-dd3a187f47aa5a0ce6d99caff64a1b4b"' :
                                            'id="xs-components-links-module-AppModule-dd3a187f47aa5a0ce6d99caff64a1b4b"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotFoundComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotFoundComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TestTabComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TestTabComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AppModule-dd3a187f47aa5a0ce6d99caff64a1b4b"' : 'data-target="#xs-injectables-links-module-AppModule-dd3a187f47aa5a0ce6d99caff64a1b4b"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AppModule-dd3a187f47aa5a0ce6d99caff64a1b4b"' :
                                        'id="xs-injectables-links-module-AppModule-dd3a187f47aa5a0ce6d99caff64a1b4b"' }>
                                        <li class="link">
                                            <a href="injectables/ApplicationService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ApplicationService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/AziendaService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AziendaService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ClassificazioneService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ClassificazioneService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/ConfirmationDialogService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>ConfirmationDialogService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/GlobalErrorHandlerService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>GlobalErrorHandlerService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MessageCacheService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MessageCacheService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/MessageService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>MessageService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/PermissionService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>PermissionService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/RoleService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>RoleService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/TipoPagamentoService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>TipoPagamentoService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UserService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link">AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/CoreModule.html" data-type="entity-link">CoreModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-CoreModule-bbf8cd8e726b879f7c19655b71532bd7"' : 'data-target="#xs-injectables-links-module-CoreModule-bbf8cd8e726b879f7c19655b71532bd7"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-CoreModule-bbf8cd8e726b879f7c19655b71532bd7"' :
                                        'id="xs-injectables-links-module-CoreModule-bbf8cd8e726b879f7c19655b71532bd7"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/RequestCacheWithMap.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>RequestCacheWithMap</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/DashboardModule.html" data-type="entity-link">DashboardModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-DashboardModule-dd1428f22a0da9b670b0470d1fe24dc6"' : 'data-target="#xs-components-links-module-DashboardModule-dd1428f22a0da9b670b0470d1fe24dc6"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-DashboardModule-dd1428f22a0da9b670b0470d1fe24dc6"' :
                                            'id="xs-components-links-module-DashboardModule-dd1428f22a0da9b670b0470d1fe24dc6"' }>
                                            <li class="link">
                                                <a href="components/ConvenzioniresultComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConvenzioniresultComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/Dashboard1Component.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">Dashboard1Component</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/Dashboard2Component.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">Dashboard2Component</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InfocardComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InfocardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NotificationsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NotificationsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ScadenzeresultComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ScadenzeresultComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TaskListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TaskListComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-DashboardModule-dd1428f22a0da9b670b0470d1fe24dc6"' : 'data-target="#xs-injectables-links-module-DashboardModule-dd1428f22a0da9b670b0470d1fe24dc6"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-DashboardModule-dd1428f22a0da9b670b0470d1fe24dc6"' :
                                        'id="xs-injectables-links-module-DashboardModule-dd1428f22a0da9b670b0470d1fe24dc6"' }>
                                        <li class="link">
                                            <a href="injectables/DashboardService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>DashboardService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/NotificationService.html"
                                                data-type="entity-link" data-context="sub-entity" data-context-id="modules" }>NotificationService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link">SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-75a4edf013df0db51ddc818661a9df7d"' : 'data-target="#xs-components-links-module-SharedModule-75a4edf013df0db51ddc818661a9df7d"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-75a4edf013df0db51ddc818661a9df7d"' :
                                            'id="xs-components-links-module-SharedModule-75a4edf013df0db51ddc818661a9df7d"' }>
                                            <li class="link">
                                                <a href="components/AccordionInfoWrapperComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccordionInfoWrapperComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AccordionWrapperComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">AccordionWrapperComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BaseEntityComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BaseEntityComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BaseResearchComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BaseResearchComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BlankComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BlankComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/BreadcrumbComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">BreadcrumbComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CollapseWrapperComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">CollapseWrapperComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ConfirmationDialogComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ConfirmationDialogComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ControlGenericListComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ControlGenericListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DatepickerTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DatepickerTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DynamicFormComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DynamicFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/DynamicTableComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">DynamicTableComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ExternalTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ExternalTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ExternalobjTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ExternalobjTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ExternalqueryComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ExternalqueryComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormInfraComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FormInfraComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormlyFieldButton.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FormlyFieldButton</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormlyFieldTemplate.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FormlyFieldTemplate</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormlyFieldTypeahead.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FormlyFieldTypeahead</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FormlyHorizontalWrapper.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FormlyHorizontalWrapper</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/FullComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">FullComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GenericTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GenericTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GridFormlyCellComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GridFormlyCellComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GridTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">GridTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/InputFileComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">InputFileComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LookupComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">LookupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/MessageComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MessageComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavigationComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavigationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavstepperWrapperComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">NavstepperWrapperComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PanelWrapperComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PanelWrapperComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PdfInfraComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PdfInfraComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PdfTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PdfTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/PdfTypeInputComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">PdfTypeInputComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/QueryBuilderComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">QueryBuilderComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RepeatTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RepeatTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RightaddonsWrapperComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">RightaddonsWrapperComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SelectTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SelectTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ShowErrorsComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">ShowErrorsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SidebarComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SidebarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SystemErrorComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">SystemErrorComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TabTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TabTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TableGroupTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TableGroupTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TableLookupTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TableLookupTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TableTypeComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TableTypeComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/TooltipWrapperComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">TooltipWrapperComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UserLoginComponent.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">UserLoginComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-SharedModule-75a4edf013df0db51ddc818661a9df7d"' : 'data-target="#xs-pipes-links-module-SharedModule-75a4edf013df0db51ddc818661a9df7d"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-75a4edf013df0db51ddc818661a9df7d"' :
                                            'id="xs-pipes-links-module-SharedModule-75a4edf013df0db51ddc818661a9df7d"' }>
                                            <li class="link">
                                                <a href="pipes/MycurrencyPipe.html"
                                                    data-type="entity-link" data-context="sub-entity" data-context-id="modules">MycurrencyPipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/AppConstants.html" data-type="entity-link">AppConstants</a>
                            </li>
                            <li class="link">
                                <a href="classes/ArrayControl.html" data-type="entity-link">ArrayControl</a>
                            </li>
                            <li class="link">
                                <a href="classes/ControlBase.html" data-type="entity-link">ControlBase</a>
                            </li>
                            <li class="link">
                                <a href="classes/ControlUtils.html" data-type="entity-link">ControlUtils</a>
                            </li>
                            <li class="link">
                                <a href="classes/DateControl.html" data-type="entity-link">DateControl</a>
                            </li>
                            <li class="link">
                                <a href="classes/DropdownControl.html" data-type="entity-link">DropdownControl</a>
                            </li>
                            <li class="link">
                                <a href="classes/FormState.html" data-type="entity-link">FormState</a>
                            </li>
                            <li class="link">
                                <a href="classes/GridModel.html" data-type="entity-link">GridModel</a>
                            </li>
                            <li class="link">
                                <a href="classes/InfraMessage.html" data-type="entity-link">InfraMessage</a>
                            </li>
                            <li class="link">
                                <a href="classes/Page.html" data-type="entity-link">Page</a>
                            </li>
                            <li class="link">
                                <a href="classes/PagedData.html" data-type="entity-link">PagedData</a>
                            </li>
                            <li class="link">
                                <a href="classes/RequestCache.html" data-type="entity-link">RequestCache</a>
                            </li>
                            <li class="link">
                                <a href="classes/TextboxControl.html" data-type="entity-link">TextboxControl</a>
                            </li>
                            <li class="link">
                                <a href="classes/TranslateExtension.html" data-type="entity-link">TranslateExtension</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/ApplicationService.html" data-type="entity-link">ApplicationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link">AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AziendaLocService.html" data-type="entity-link">AziendaLocService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/AziendaService.html" data-type="entity-link">AziendaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/BaseService.html" data-type="entity-link">BaseService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ClassificazioneService.html" data-type="entity-link">ClassificazioneService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ConfirmationDialogService.html" data-type="entity-link">ConfirmationDialogService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/CoreSevice.html" data-type="entity-link">CoreSevice</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DashboardService.html" data-type="entity-link">DashboardService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DocumentoService.html" data-type="entity-link">DocumentoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/DynamicTableComponent.html" data-type="entity-link">DynamicTableComponent</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/GlobalErrorHandlerService.html" data-type="entity-link">GlobalErrorHandlerService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LogAttivitaService.html" data-type="entity-link">LogAttivitaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MappingRuoloService.html" data-type="entity-link">MappingRuoloService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MappingUfficioService.html" data-type="entity-link">MappingUfficioService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MessageCacheService.html" data-type="entity-link">MessageCacheService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/MessageService.html" data-type="entity-link">MessageService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NgbDateCustomParserFormatter.html" data-type="entity-link">NgbDateCustomParserFormatter</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NgbStringAdapter.html" data-type="entity-link">NgbStringAdapter</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/NotificationService.html" data-type="entity-link">NotificationService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PermissionService.html" data-type="entity-link">PermissionService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/PersonaInternaService.html" data-type="entity-link">PersonaInternaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RequestCacheWithMap.html" data-type="entity-link">RequestCacheWithMap</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/RoleService.html" data-type="entity-link">RoleService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ScadenzaService.html" data-type="entity-link">ScadenzaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StrutturaEsternaService.html" data-type="entity-link">StrutturaEsternaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/StrutturaInternaService.html" data-type="entity-link">StrutturaInternaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/TipoPagamentoService.html" data-type="entity-link">TipoPagamentoService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UnitaOrganizzativaService.html" data-type="entity-link">UnitaOrganizzativaService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserService.html" data-type="entity-link">UserService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserTaskService.html" data-type="entity-link">UserTaskService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/CachingInterceptor.html" data-type="entity-link">CachingInterceptor</a>
                            </li>
                            <li class="link">
                                <a href="interceptors/TokenInterceptor.html" data-type="entity-link">TokenInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link">AuthGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/LoginActivate.html" data-type="entity-link">LoginActivate</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/BaseEntity.html" data-type="entity-link">BaseEntity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Convenzione.html" data-type="entity-link">Convenzione</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Entity.html" data-type="entity-link">Entity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/EntityMap.html" data-type="entity-link">EntityMap</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Field.html" data-type="entity-link">Field</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FieldMap.html" data-type="entity-link">FieldMap</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FileAttachment.html" data-type="entity-link">FileAttachment</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/FileInfra.html" data-type="entity-link">FileInfra</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IDoc.html" data-type="entity-link">IDoc</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IDoc-1.html" data-type="entity-link">IDoc</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IDoc-2.html" data-type="entity-link">IDoc</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IInfoApprovazione.html" data-type="entity-link">IInfoApprovazione</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IInfoRepertorio.html" data-type="entity-link">IInfoRepertorio</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IInfoSottoscrizione.html" data-type="entity-link">IInfoSottoscrizione</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/IQueryMetadata.html" data-type="entity-link">IQueryMetadata</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/LoginResponse.html" data-type="entity-link">LoginResponse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Operator.html" data-type="entity-link">Operator</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Option.html" data-type="entity-link">Option</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Owner.html" data-type="entity-link">Owner</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/QueryBuilderConfig.html" data-type="entity-link">QueryBuilderConfig</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RequestCacheEntry.html" data-type="entity-link">RequestCacheEntry</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ResultParse.html" data-type="entity-link">ResultParse</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RouteInfo.html" data-type="entity-link">RouteInfo</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Rule.html" data-type="entity-link">Rule</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/RuleSet.html" data-type="entity-link">RuleSet</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ServiceBase.html" data-type="entity-link">ServiceBase</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ServiceEntity.html" data-type="entity-link">ServiceEntity</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/ServiceQuery.html" data-type="entity-link">ServiceQuery</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/StepType.html" data-type="entity-link">StepType</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});